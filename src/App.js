import './App.css';
import React from "react";
import ProductsAll from "./components/ProductsAll/ProductsAll";
import Header from "./components/Header/Header";

class App extends React.Component {
    state = {
        productsData: [],
        cartCount: 0,
        favoriteCount: 0,
    }
    incrementCartCount = () => {
        this.setState(current => {
            localStorage.setItem('cartCount', current.cartCount + 1);
            return {cartCount: current.cartCount + 1}
        })
    }
    incrementFavoriteCount = (num) => {
        this.setState(current => {
            localStorage.setItem('favoriteCount', current.favoriteCount + num);
            return {favoriteCount: current.favoriteCount + num}
        })
    }

    async componentDidMount() {
        const productsDataInLocalStorage = localStorage.getItem("productsData");
        const productsData = JSON.parse(productsDataInLocalStorage);
        if (productsData) {
            this.setState({ productsData: productsData });
        } else {
            const result = await fetch("./products/products.json").then(res => res.json());
            localStorage.setItem("productsData", JSON.stringify(result));
            this.setState({ productsData: result });
        }
        if (localStorage.getItem('cartArr')) {
            this.setState({ cartCount: +JSON.parse(localStorage.getItem('cartArr')).length})
        } else {
            this.setState({ cartCount: this.state.cartCount })
        }
        if (localStorage.getItem('favoriteArr')) {
            this.setState({ favoriteCount: +JSON.parse(localStorage.getItem('favoriteArr')).length})
        } else {
            this.setState({ favoriteCount: this.state.favoriteCount })
        }
    }

    render() {
        return (
            <div className="App">
                <Header
                    favoriteCount = {this.state.favoriteCount}
                    cartCount = {this.state.cartCount}
                />
                <ProductsAll
                    products={this.state.productsData}
                    incrementCartCount={this.incrementCartCount}
                    incrementFavoriteCount={this.incrementFavoriteCount}
                />
            </div>
        );
    }
}

export default App;