import React from 'react';
import styles from './Header.module.scss';
import PropTypes from "prop-types";
import { ReactComponent as StarEmpty } from "../../assets/svg/star-empty.svg";
import { ReactComponent as CartIcon } from "../../assets/svg/cart-icon.svg";

class Header extends React.PureComponent {

    render() {
        const { favoriteCount, cartCount } = this.props;
        return (
            <header className={styles.header}>
                <div className={styles.logo}>
                    <a className={styles.logoTextLink} href="./" target="_blank">KitGoods</a>
                </div>
                <div className={styles.countersWrapper}>
                 <div className={styles.countContainer}>
                    <a href="./" target="_blank"><StarEmpty className={styles.favoriteSvg} /></a>
                     <p>{favoriteCount}</p>
                 </div>
                 <div className={styles.countContainer}>
                     <a href="./" target="_blank"><CartIcon className={styles.cartSvg} /></a>
                     <p>{cartCount}</p>
                 </div>
             </div>
            </header>
        )
    }
}

Header.propTypes = {
    favoriteCount: PropTypes.number,
    cartCount: PropTypes.number,
};

Header.defaultProps = {
    favoriteCount: 0,
    cartCount: 0,
};

export default Header;