import React from 'react';
import styles from './Modal.module.scss';
import PropTypes from "prop-types";

class Modal extends React.PureComponent {

  render(){
      const { header, closeButton, text, LeftBtnTxt, RightBtnTxt, bgColor, closeModal, actions } = this.props;
      return(
          <div className={styles.modal}>
              <div className={styles.modalBackground}  onClick={closeModal}></div>
              <div className={styles.modalMainContainer} style={{backgroundColor: bgColor}}>
                  <header className={styles.modalHeader}>{header}</header>
                  {closeButton && <button className={styles.modalClose} onClick={closeModal}>&#9587;</button>}
                  <div className={styles.modalContentWrapper}>{text}</div>
                  <div className={styles.modalButtonWrapper}>
                      <button className={styles.modalButtonLeft} onClick={actions}>{LeftBtnTxt}</button>
                      <button className={styles.modalButtonRight} onClick={closeModal}>{RightBtnTxt}</button>
                  </div>
              </div>
          </div>
      )
 }
}

Modal.propTypes = {
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    LeftBtnTxt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    RightBtnTxt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    bgColor: PropTypes.string,
    closeModal: PropTypes.func,
    actions: PropTypes.func
};

Modal.defaultProps = {
    closeButton: true,
    bgColor:"#64B743",
    closeModal: () => {},
    actions: () => {}
};

export default Modal;