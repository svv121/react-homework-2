import React from 'react';
import styles from './Button.module.scss';
import PropTypes from "prop-types";

class Button extends React.PureComponent {

	render() {
		const { backgroundColor, handleClick, text } = this.props;
		return (
			<button className={styles.buttonsMain} style={{backgroundColor:backgroundColor}} onClick={handleClick}>
				{text}
			</button>
		)
	}
}

Button.propTypes = {
	text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	backgroundColor: PropTypes.string,
	handleClick: PropTypes.func
};

Button.defaultProps = {
	backgroundColor: "#64B743",
	handleClick: () => {}
};

export default Button;