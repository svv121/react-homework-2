import React, { PureComponent } from "react";
import Product from "../Product/Product";
import styles from "./ProductsAll.module.scss";
import PropTypes from "prop-types";

class ProductsAll extends PureComponent {
  state = {
    favoriteArr: JSON.parse(localStorage.getItem('favoriteArr')) || []
  }
  changeFavoriteArr = (id) => {
    const { favoriteArr } = this.state;
    const index = favoriteArr.indexOf(id);
    const newFavoriteArr = [...favoriteArr];
    if(index !== -1) {
      newFavoriteArr.splice(index,1);
    } else {
      newFavoriteArr.push(id);
    }
    this.setState({favoriteArr: newFavoriteArr},
        () => localStorage.setItem('favoriteArr', JSON.stringify(newFavoriteArr)))
  }

  render() {
    const {products, incrementCartCount, incrementFavoriteCount} = this.props;
    const{favoriteArr} = this.state

    return (
      <>
        <h1 className={styles.ProductsAllTitle}>Latest arrivals in KitGoods</h1>
        <div className={styles.ProductsAll}>
          {products.map(({ id, isFavorite, price, currency, article, ...args }) => (
            <Product
                incrementCartCount={incrementCartCount}
                incrementFavoriteCount={incrementFavoriteCount}
                key={id}
                id={parseInt(id)}
                price={parseFloat(price)}
                currency="$"
                article={parseInt(article)}
                changeFavoriteArr={this.changeFavoriteArr}
                isFavorite={favoriteArr.indexOf(id) !== -1}
                {...args}
            />
          ))}
        </div>
      </>
    );
  }
}

ProductsAll.propTypes = {
  products: PropTypes.array,
  incrementCartCount: PropTypes.func,
  incrementFavoriteCount: PropTypes.func,

};

ProductsAll.defaultProps = {
  products: [],
  incrementCartCount: () => {},
  incrementFavoriteCount:  () => {}
};

export default ProductsAll;